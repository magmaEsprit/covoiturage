<?php

namespace Projet\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annonces
 *
 * @ORM\Table(name="annonces")
 * @ORM\Entity
 */
class Annonces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_annonce", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAnnonce;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_depart", type="string", length=30, nullable=false)
     */
    private $villeDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_arriver", type="string", length=30, nullable=false)
     */
    private $villeArriver;

    /**
     * @var integer
     *
     * @ORM\Column(name="cout", type="integer", nullable=true)
     */
    private $cout;

    /**
     * @var string
     *
     * @ORM\Column(name="lieux_depart", type="string", length=30, nullable=true)
     */
    private $lieuxDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="lieux_arriver", type="string", length=30, nullable=true)
     */
    private $lieuxArriver;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_places_dispo", type="integer", nullable=true)
     */
    private $nbrPlacesDispo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_depart", type="date", nullable=true)
     */
    private $dateDepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arriver", type="date", nullable=true)
     */
    private $dateArriver;

    /**
     * @var string
     *
     * @ORM\Column(name="bagage", type="string", length=30, nullable=true)
     */
    private $bagage;

    /**
     * @var string
     *
     * @ORM\Column(name="colis", type="string", length=30, nullable=true)
     */
    private $colis;

    /**
     * @var string
     *
     * @ORM\Column(name="handicap", type="string", length=30, nullable=true)
     */
    private $handicap;

    /**
     * @var string
     *
     * @ORM\Column(name="animaux", type="string", length=30, nullable=true)
     */
    private $animaux;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe_passagers", type="string", length=30, nullable=true)
     */
    private $sexePassagers;


}

