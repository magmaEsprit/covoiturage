<?php

namespace Projet\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cat�gorie
 *
 * @ORM\Table(name="catégorie")
 * @ORM\Entity
 */
class Cat�gorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_catégorie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCat�gorie;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_catégorie", type="string", length=50, nullable=false)
     */
    private $libelleCat�gorie;


}

