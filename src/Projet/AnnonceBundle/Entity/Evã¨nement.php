<?php

namespace Projet\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ev�nement
 *
 * @ORM\Table(name="evènement")
 * @ORM\Entity
 */
class Ev�nement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_evènement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEv�nement;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_de_debut", type="date", nullable=false)
     */
    private $dateDeDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_de_fin", type="date", nullable=false)
     */
    private $dateDeFin;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_évènement", type="string", length=50, nullable=false)
     */
    private $nomév�nement;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=60, nullable=false)
     */
    private $siteWeb;


}

