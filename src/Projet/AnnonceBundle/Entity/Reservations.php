<?php

namespace Projet\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservations
 *
 * @ORM\Table(name="reservations")
 * @ORM\Entity
 */
class Reservations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_reservation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReservation;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_annonce", type="integer", nullable=false)
     */
    private $idAnnonce;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_places_reservees", type="integer", nullable=true)
     */
    private $nbrPlacesReservees;

    /**
     * @var string
     *
     * @ORM\Column(name="itineraire_souhaite", type="string", length=30, nullable=true)
     */
    private $itineraireSouhaite;

    /**
     * @var string
     *
     * @ORM\Column(name="baggage", type="string", length=20, nullable=false)
     */
    private $baggage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_colis", type="integer", nullable=false)
     */
    private $idColis;

    /**
     * @var string
     *
     * @ORM\Column(name="handicape", type="string", length=20, nullable=false)
     */
    private $handicape;


}

